---
date: 2022-01-25
title: Resource-Efficient Software & Blauer Engel Eco-Certification
categories:  [Free Software, Energy Efficiency, Hardware Operating Life, Blauer Engel]
author: Joseph P. De Veaugh-Geiss
summary: Software efficiency is resource efficiency!
aliases:
  - ./post-1/
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
SPDX-License-Identifier: CC-BY-4.0
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
---

Often overlooked is the fact that the resource consumption of hardware is determined by the software running on it.

*How much energy does hardware use?*

Take a look at the software: the same machine doing the same task but with two different applications can have [drastically different energy demands](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

*How long is the operating life of a piece of hardware?*

Again, software is critical. Software bloat requiring more and more powerful hardware results in new devices being manufactured and sold, while perfectly functioning devices are discarded.

{{< container class="text-center" >}}

![WEEE Man At The Eden Project](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man At The Eden Project")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

So when thinking about what to do while the climate crisis continues, where to start and how, we at [KDE Eco](https://eco.kde.org/) think software -- especially Free Software -- is a good place to start. Free Software means users and their communities have the freedom to control the software they use, not the other way around. These freedoms provide real choice. Choice in what to install, or uninstall. Options to modify software to run more efficiently. Continuing support for older, but perfectly functioning, devices. And so on.

Each of these choices has a cost and/or benefit. For instance, the more (unnecessary) processes an application has running in the background, the more resources the hardware will need. Multiply such costs by hundreds of millions, or even billions, of computer users worldwide, and it quickly adds up.

The opposite is also true: reduce the demands from software and one reduces the demands on our resources.

In other words, software efficiency means resource efficiency!

Recently, the interdependency between software engineering and sustainability was officially recognized by the [German Environment Agency](https://www.umweltbundesamt.de/en) (*Umweltbundesamt*, or UBA). In 2020, UBA released the [award criteria](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) for desktop software to obtain eco-certification with the Blauer Engel label. Categories for certification include energy efficiency, extending the potential operating life of hardware, and user autonomy ... all of which fit seamlessly with Free and Open Source Software (FOSS).

{{< container class="text-center" >}}

![KDE Eco logo with vegetation](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

The Blauer Engel 4 FOSS ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) project from KDE e.V. seeks to collect, summarize, and spread information related to Blauer Engel eco-certification and resource efficiency as it relates to FOSS development. Measuring the energy consumption of Free Software is the focus of the Free & Open Source Software Energy Efficiency Project ([FEEP](https://invent.kde.org/teams/eco/feep)). Both projects are part of the pioneering [KDE Eco](https://invent.kde.org/teams/eco) initiative!

{{< container class="text-center" >}}

<font size="6"> **[Join us in building energy-efficient, Free Software!](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*This is a modified version of the abstract [submitted](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) to the Remote Rhein Ruhr Stage at CCC 2021. You can watch the talk [here](https://streaming.media.ccc.de/rc3/relive/319).*

#### Funding Notice

The BE4FOSS project was funded by the Federal Environment Agency and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>). The funds are made available by resolution of the German Bundestag.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

The publisher is responsible for the content of this publication.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
